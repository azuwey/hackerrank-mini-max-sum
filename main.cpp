/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/20/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/mini-max-sum/problem
 **/
#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

void miniMaxSum(vector<int> arr) {
  int64_t minsum = 0;
  int64_t maxsum = 0;

  // Check every element of the vector are equal in our array
  if (std::adjacent_find(arr.begin(), arr.end(), std::not_equal_to<>()) ==
      arr.end()) {
    // Calculate outputs
    minsum = maxsum = accumulate(arr.begin(), arr.end(), 0) - arr[0];

    // Output our values
    cout << minsum << " " << maxsum << endl;
  } else {
    // Find the minimum and maximum
    auto minmax_c = minmax_element(arr.begin(), arr.end());

    // Sum each number to the sum vector
    for_each(arr.begin(), arr.end(), [&minsum, &maxsum, &minmax_c](int v) {
      // Check the first value in the min to max sorted array
      // is equal with the current value
      if (v != minmax_c.first[0]) {
        maxsum += v;
      }

      // Check the first value in the max to min sorted array
      // is equal with the current value
      if (v != minmax_c.second[0]) {
        minsum += v;
      }
    });

    // Output our values
    cout << minsum << " " << maxsum << endl;
  }
}

int main() {
  string arr_temp_temp;
  getline(cin, arr_temp_temp);

  vector<string> arr_temp = split_string(arr_temp_temp);

  vector<int> arr(5);

  for (int i = 0; i < 5; i++) {
    int arr_item = stoi(arr_temp[i]);

    arr[i] = arr_item;
  }

  miniMaxSum(arr);

  return 0;
}

vector<string> split_string(string input_string) {
  string::iterator new_end =
      unique(input_string.begin(), input_string.end(),
             [](const char &x, const char &y) { return x == y and x == ' '; });

  input_string.erase(new_end, input_string.end());

  while (input_string[input_string.length() - 1] == ' ') {
    input_string.pop_back();
  }

  vector<string> splits;
  char delimiter = ' ';

  size_t i = 0;
  size_t pos = input_string.find(delimiter);

  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));

    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }

  splits.push_back(
      input_string.substr(i, min(pos, input_string.length()) - i + 1));

  return splits;
}
